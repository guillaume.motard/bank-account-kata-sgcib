package operation;

import java.util.List;
import java.util.Optional;

public interface OperationsRepository {
    Optional<Operation> getLastOperation(int accountId);

    void save(int accountId, Operation operation);

    List<Operation> getOperations(int accountId);
}
