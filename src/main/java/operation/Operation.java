package operation;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Operation(LocalDateTime date, int accountId, OperationType type, BigDecimal amount, BigDecimal balance) {
}
