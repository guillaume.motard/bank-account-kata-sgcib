package exception;

public class AccountOverdraftException extends Throwable {
    public AccountOverdraftException(final String message) {
        super(message);
    }
}
