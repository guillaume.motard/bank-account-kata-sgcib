package statement;

import java.util.List;

public interface StatementPrinter {
    void print(List<String> statementLines);
}
