package statement;

import java.io.PrintStream;
import java.util.List;

public class PrintStreamStatementPrinter implements StatementPrinter {
    private final PrintStream printStream;

    public PrintStreamStatementPrinter(final PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void print(final List<String> statementLines) {
        for (final var statementLine : statementLines) {
            printStream.println(statementLine);
        }
    }
}
