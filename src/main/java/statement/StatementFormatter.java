package statement;

import operation.Operation;

import java.util.List;

public interface StatementFormatter {
    List<String> format(List<Operation> operations);
}
