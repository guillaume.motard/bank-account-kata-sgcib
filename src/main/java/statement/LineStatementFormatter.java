package statement;

import operation.Operation;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class LineStatementFormatter implements StatementFormatter {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final DecimalFormat decimalFormat = new DecimalFormat("0.00");

    @Override
    public List<String> format(final List<Operation> operations) {
        return operations
                .stream()
                .map(operation -> "" + operation.date().format(dateTimeFormatter) + " | " + operation.type() + " | " + decimalFormat.format(operation.amount()) + " | " + decimalFormat.format(operation.balance()))
                .toList();
    }
}
