package bankAccount;

import exception.AccountOverdraftException;
import operation.Operation;
import operation.OperationType;
import operation.OperationsRepository;
import statement.StatementFormatter;
import statement.StatementPrinter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.LocalDateTime;

public class OperationBasedBankAccount implements BankAccount {
    private final OperationsRepository operationsRepository;

    private final Clock clock;

    private final StatementFormatter statementFormatter;

    private final StatementPrinter statementPrinter;

    public OperationBasedBankAccount(final OperationsRepository operationsRepository, final Clock clock, final StatementFormatter statementFormatter, final StatementPrinter statementPrinter) {
        this.operationsRepository = operationsRepository;
        this.clock = clock;
        this.statementFormatter = statementFormatter;
        this.statementPrinter = statementPrinter;
    }

    @Override
    public void deposit(final int accountId, final BigDecimal amount) {
        final var scaledAmount = amount.setScale(2, RoundingMode.HALF_EVEN);
        if (scaledAmount.compareTo(BigDecimal.ZERO) <= 0 ) {
            throw new IllegalArgumentException("The amount should be positive");
        }
        final var currentBalance = operationsRepository
                .getLastOperation(accountId)
                .map(Operation::balance)
                .orElse(BigDecimal.ZERO);

        final var operation = new Operation(
                LocalDateTime.now(clock), accountId, OperationType.DEPOSIT, scaledAmount, currentBalance.add(scaledAmount)
        );
        operationsRepository.save(accountId, operation);
    }

    @Override
    public void withdraw(final int accountId, final BigDecimal amount) throws AccountOverdraftException {
        final var scaledAmount = amount.setScale(2, RoundingMode.HALF_EVEN);

        if (scaledAmount.compareTo(BigDecimal.ZERO) <= 0 ) {
            throw new IllegalArgumentException("The amount should be positive");
        }

        final var currentBalance = operationsRepository
                .getLastOperation(accountId)
                .map(Operation::balance)
                .orElse(BigDecimal.ZERO);

        if (currentBalance.compareTo(scaledAmount) < 0) {
            throw new AccountOverdraftException("You don't have enough money to make this withdraw");
        }

        final var operation = new Operation(
                LocalDateTime.now(clock), accountId, OperationType.WITHDRAW, scaledAmount, currentBalance.subtract(scaledAmount)
        );

        operationsRepository.save(accountId, operation);
    }

    @Override
    public void printStatement(final int accountId) {
        final var operations = operationsRepository.getOperations(accountId);
        final var operationsFormatted = statementFormatter.format(operations);
        statementPrinter.print(operationsFormatted);
    }
}
