package bankAccount;

import exception.AccountOverdraftException;

import java.math.BigDecimal;

public interface BankAccount {
    void deposit(final int accountId, final BigDecimal amount);

    void withdraw(final int accountId, final BigDecimal amount) throws AccountOverdraftException;

    void printStatement(final int accountId);
}
