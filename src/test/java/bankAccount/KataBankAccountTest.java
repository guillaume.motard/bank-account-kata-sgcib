package bankAccount;

import exception.AccountOverdraftException;
import operation.InMemoryOperationsRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import statement.LineStatementFormatter;
import statement.PrintStreamStatementPrinter;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class KataBankAccountTest {
    private final PrintStream systemOut = System.out;
    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(systemOut);
    }

    @Test
    void integrationTest() throws AccountOverdraftException {
        final var inMemoryOperationsRepository = new InMemoryOperationsRepository();
        final var clock = Clock.fixed(Instant.parse("2022-07-25T16:02:42.00Z"), ZoneId.systemDefault());
        final var lineStatementFormatter = new LineStatementFormatter();
        final var printStreamStatementPrinter = new PrintStreamStatementPrinter(new PrintStream(byteArrayOutputStream));
        final var operationsBasedBankAccount = new OperationBasedBankAccount(inMemoryOperationsRepository, clock, lineStatementFormatter, printStreamStatementPrinter);
        final var endOfLine = System.lineSeparator();
        final var expected = "2022-07-25 18:02:42 | DEPOSIT | 24,00 | 24,00" + endOfLine +
                "2022-07-25 18:02:42 | DEPOSIT | 10,00 | 34,00" + endOfLine +
                "2022-07-25 18:02:42 | WITHDRAW | 34,00 | 0,00" + endOfLine +
                "2022-07-25 18:02:42 | DEPOSIT | 3000,00 | 3000,00" + endOfLine +
                "2022-07-25 18:02:42 | WITHDRAW | 2500,00 | 500,00" + endOfLine;

        operationsBasedBankAccount.deposit(1, new BigDecimal(24));
        operationsBasedBankAccount.deposit(1, BigDecimal.TEN);
        operationsBasedBankAccount.withdraw(1, new BigDecimal(34));
        operationsBasedBankAccount.deposit(1, new BigDecimal(3000));
        operationsBasedBankAccount.withdraw(1, new BigDecimal(2500));
        operationsBasedBankAccount.printStatement(1);

        final var result = byteArrayOutputStream.toString();
        assertEquals(expected, result);
    }
}
