package bankAccount;

import exception.AccountOverdraftException;
import operation.Operation;
import operation.OperationType;
import operation.OperationsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import statement.StatementFormatter;
import statement.StatementPrinter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BankAccountTests {
    @Mock
    private OperationsRepository operationsRepository;

    @Mock
    private StatementPrinter statementPrinter;

    @Mock
    private StatementFormatter statementFormatter;

    private OperationBasedBankAccount bankAccount;

    private final Clock clock = Clock.fixed(Instant.parse("2022-07-25T16:02:42.00Z"), ZoneId.systemDefault());

    @BeforeEach
    void setup() {
        bankAccount = new OperationBasedBankAccount(operationsRepository, clock, statementFormatter, statementPrinter);
    }

    @Test
    void shouldDepositAValidAmount() {
        final var amount = new BigDecimal(5).setScale(2, RoundingMode.HALF_EVEN);
        final var dateTime = LocalDateTime.now(clock);
        final var expected = new Operation(dateTime, 1, OperationType.DEPOSIT, amount, new BigDecimal(15).setScale(2, RoundingMode.HALF_EVEN));

        when(operationsRepository.getLastOperation(1)).thenReturn(Optional.of(new Operation(dateTime, 1, OperationType.DEPOSIT, BigDecimal.ONE.setScale(2, RoundingMode.HALF_EVEN), BigDecimal.TEN.setScale(2, RoundingMode.HALF_EVEN))));
        bankAccount.deposit(1, amount);

        final var orderVerifier = inOrder(operationsRepository);
        orderVerifier.verify(operationsRepository).getLastOperation(1);
        orderVerifier.verify(operationsRepository).save(1, expected);
        orderVerifier.verifyNoMoreInteractions();
    }

    @Test
    void shouldThrowExceptionForADepositWithANegativeAmount() {
        final var amount = new BigDecimal(-5);

        final var thrown = assertThrows(IllegalArgumentException.class, () -> bankAccount.deposit(1, amount));

        assertEquals("The amount should be positive", thrown.getMessage());
    }

    @Test
    void shouldWithdrawAValidAmount() throws AccountOverdraftException {
        final var amount = new BigDecimal(5).setScale(2, RoundingMode.HALF_EVEN);
        final var now = LocalDateTime.now(clock);
        final var expected = new Operation(now, 1, OperationType.WITHDRAW, amount, BigDecimal.TEN.setScale(2, RoundingMode.HALF_EVEN));

        when(operationsRepository.getLastOperation(1)).thenReturn(Optional.of(new Operation(now, 1, OperationType.WITHDRAW, BigDecimal.TEN.setScale(2, RoundingMode.HALF_EVEN), new BigDecimal(15).setScale(2, RoundingMode.HALF_EVEN))));
        bankAccount.withdraw(1, amount);

        final var orderVerifier = inOrder(operationsRepository);
        orderVerifier.verify(operationsRepository).getLastOperation(1);
        orderVerifier.verify(operationsRepository).save(1, expected);
        orderVerifier.verifyNoMoreInteractions();
    }

    @Test
    void shouldThrowExceptionForAWithdrawWithANegativeAmount() {
        final var amount = new BigDecimal(-5);

        final var thrown = assertThrows(IllegalArgumentException.class, () -> bankAccount.withdraw(1, amount));

        assertEquals("The amount should be positive", thrown.getMessage());
    }

    @Test
    void withdrawAmountBiggerThanBalanceShouldThrowError() {
        final var amount = BigDecimal.TEN;

        when(operationsRepository.getLastOperation(1)).thenReturn(Optional.of(new Operation(LocalDateTime.now(clock), 1, OperationType.WITHDRAW, BigDecimal.TEN, BigDecimal.ZERO)));
        final var thrown = assertThrows(AccountOverdraftException.class, () -> bankAccount.withdraw(1, amount));

        assertEquals("You don't have enough money to make this withdraw", thrown.getMessage());
    }

    @Test
    void shouldPrintStatement() {
        final var operations = List.of(
                new Operation(LocalDateTime.now(clock), 1, OperationType.DEPOSIT, BigDecimal.TEN, new BigDecimal(106)),
                new Operation(LocalDateTime.now(clock), 1, OperationType.WITHDRAW, BigDecimal.TEN, new BigDecimal(96))
        );
        final var operationsFormatted = List.of(
                "2022-07-25T16:02:42.00Z | Deposit | 10 | 106",
                "2022-07-25T16:02:42.00Z | Withdraw | 10 | 96"
        );

        when(operationsRepository.getOperations(1)).thenReturn(operations);
        when(statementFormatter.format(operations)).thenReturn(operationsFormatted);
        bankAccount.printStatement(1);

        final var orderVerifier = inOrder(operationsRepository, statementFormatter, statementPrinter);
        orderVerifier.verify(operationsRepository).getOperations(1);
        orderVerifier.verify(statementFormatter).format(operations);
        orderVerifier.verify(statementPrinter).print(operationsFormatted);
        orderVerifier.verifyNoMoreInteractions();
    }
}
