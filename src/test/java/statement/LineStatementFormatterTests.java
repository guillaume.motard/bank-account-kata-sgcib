package statement;

import operation.Operation;
import operation.OperationType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class LineStatementFormatterTests {
    private final StatementFormatter statementFormatter = new LineStatementFormatter();

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Test
    void shouldFormatOperations() {
        final var date = LocalDateTime.now();

        final var operations = List.of(
                new Operation(date, 1, OperationType.DEPOSIT, BigDecimal.TEN, new BigDecimal(106)),
                new Operation(date, 1, OperationType.WITHDRAW, BigDecimal.TEN, new BigDecimal(96))
        );
        final var operationsFormatted = List.of(
                date.format(dateTimeFormatter) + " | DEPOSIT | 10,00 | 106,00",
                date.format(dateTimeFormatter) + " | WITHDRAW | 10,00 | 96,00"
        );
        final var result = statementFormatter.format(operations);
        Assertions.assertEquals(operationsFormatted, result);
    }

    @Test
    void shouldReturnAnEmptyList() {
        final var operations = List.<Operation>of();
        final var result = statementFormatter.format(operations);
        Assertions.assertTrue(result.isEmpty());
    }
}
