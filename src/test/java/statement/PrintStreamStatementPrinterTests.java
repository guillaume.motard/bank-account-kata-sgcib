package statement;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PrintStreamStatementPrinterTests {

    @InjectMocks
    private PrintStreamStatementPrinter printStreamStatementPrinter;

    @Mock
    private PrintStream printStream;

    final Clock clock = Clock.fixed(Instant.parse("2022-07-25T16:02:42.00Z"), ZoneId.systemDefault());

    @Test
    void shouldPrintStatementLines() {
        final var dateTime = LocalDateTime.now(clock);
        final var statementLines = List.of(
                dateTime + " | DEPOSIT | 10,00 | 106,00",
                dateTime + " | WITHDRAW | 10,00 | 96,00"
        );

        printStreamStatementPrinter.print(statementLines);


        verify(printStream).println(dateTime + " | DEPOSIT | 10,00 | 106,00");
        verify(printStream).println(dateTime + " | WITHDRAW | 10,00 | 96,00");
        verifyNoMoreInteractions(printStream);
    }

    @Test
    void shouldNotPrintEmptyStatementLines() {
        final var statementsLines = List.<String>of();

        printStreamStatementPrinter.print(statementsLines);

        verifyNoInteractions(printStream);
    }
}
