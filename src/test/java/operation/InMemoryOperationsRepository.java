package operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InMemoryOperationsRepository implements OperationsRepository{
    private final List<Operation> operations = new ArrayList<>();
    @Override
    public Optional<Operation> getLastOperation(final int accountId) {
        return operations.stream().reduce((operation, lastOperation) -> lastOperation);
    }

    @Override
    public void save(final int accountId, final Operation operation) {
        operations.add(operation);
    }

    @Override
    public List<Operation> getOperations(final int accountId) {
        return operations;
    }
}
